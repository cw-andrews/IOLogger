#!/usr/bin/env bash
# This script sets-up the environment in the container.

echo "***********************************************"
echo "***************** install *********************"
echo "***********************************************"

echo "***********************************************"
echo "---apt update & upgrade---"
echo "***********************************************"
apt-get -y update

echo "***********************************************"
echo "---OS dependencies---"
echo "***********************************************"
apt-get -y install python3-pip
apt-get -y install python3-dev python3-setuptools
apt-get -y install git
apt-get -y install supervisor

echo "***********************************************"
echo "---install dependencies ---"
echo "***********************************************"
pip install --upgrade pip
pip install -r requirements.txt
pip freeze