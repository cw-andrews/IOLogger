============
IOLogger
============
Decorator which logs the wrapped function/method.

    The following are logged:

        1. name of the function called
        2. arg(s) passed for the function called (if any)
        3. kwarg(s) passed for the function called (if any)
        4. execution time of the function called (in seconds)

        * also catches and logs any **exceptions** raised *gracefully*.
