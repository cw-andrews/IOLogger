# IOLogger

##### Decorator which logs the wrapped function/method.
    The following are logged:
        1. name of the function called
        2. arg(s) passed for the function called (if any)
        3. kwarg(s) passed for the function called (if any)
        4. execution time of the function called (in seconds)
        5. also catches and logs any **exceptions** raised *gracefully*.

**Quick Installation:**

``pip install iologger``

##### **Simply attach to your functions/methods:**
```
>>> from iologger import iologger
...
...
... @iologger
... def sample_function(simple_arg: str, simple_kwarg: str='yes') -> str:
...     return simple_arg + " " + simple_kwarg
```

##### **Just setup your logging handler and it will start working:**
```
>>> from sys import stdout
...
... from logbook import StreamHandler
...
... stdout_handler = StreamHandler(stdout)
... stdout_handler.push_application()
...
>>> sample_function("test-string", simple_kwarg="no")

[2017-01-29 18:48:50.170606] DEBUG: IOL - sample_function: Starting...
[2017-01-29 18:48:50.171035] DEBUG: IOL - sample_function: passed args/kwargs = {'args': ('test-string',), 'kwargs': {'simple_kwarg': 'no'}}
[2017-01-29 18:48:50.171197] DEBUG: IOL - sample_function: returned: 'test-string no'
[2017-01-29 18:48:50.171319] INFO: IOL - sample_function: ...Finished (3.3e-06 seconds)
```
